import {
  Loadable,
  failableActionMaker,
  bothWithMeta,
  implementsInterface,
  isArrayOf,
  FailableAction,
  PayloadAction,
  payloadActionMaker,
} from '../utils';
import {
  isNumber,
  isString,
  isBoolean,
  isNull,
  isUndefined,
  isNullOrUndefined,
} from 'util';
import { function as FN } from 'fp-ts';
import { LocationChangeAction } from 'connected-react-router';

export interface IUser {
  id: number;
  login: string;
  firstName: string | null;
  lastName: string | null;
  email: string;
  imageUrl: string | null;
  activated: boolean;
  langKey: string | null;
  createdBy: string | undefined;
  createdDate: string | undefined | null;
  lastModifiedBy: string | undefined;
  lastModifiedDate: string | undefined | null;
  authorities: string[] | undefined;
}

export const emptyUser: IUser = {
  id: 0,
  login: '',
  firstName: '',
  lastName: '',
  email: '',
  imageUrl: '',
  activated: false,
  langKey: '',
  createdBy: '',
  createdDate: '',
  lastModifiedBy: '',
  lastModifiedDate: '',
  authorities: [],
};

export const isUser = implementsInterface<IUser>({
  id: isNumber,
  login: isString,
  firstName: FN.or(isString, isNull),
  lastName: FN.or(isString, isNull),
  email: isString,
  imageUrl: FN.or(isString, isNull),
  activated: isBoolean,
  langKey: FN.or(isString, isNull),
  createdBy: FN.or(isString, isUndefined),
  createdDate: FN.or(isString, isNullOrUndefined),
  lastModifiedBy: FN.or(isString, isUndefined),
  lastModifiedDate: FN.or(isString, isNullOrUndefined),
  authorities: FN.or(isArrayOf(isString), isUndefined),
});

export interface IUserDTO {
  login: string;
  firstName: string | null;
  lastName: string | null;
  email: string;
  activated: boolean;
  authorities?: string[];
}

export const emptyUserDTO: IUserDTO = {
  login: '',
  firstName: '',
  lastName: '',
  email: '',
  activated: false,
  authorities: [],
};

export interface IUsersState {
  memo: IUser[];
  users: Loadable<IUser[]>;
  query: {
    page: number;
    itemsPerPage: number;
  };
  count: number;
  deleteResult: Loadable<undefined>;
}

export type GetUsers = PayloadAction<
  'users/GET_USERS',
  Partial<{ page: number; itemsPerPage: number }>
>;

export const getUsersAction = payloadActionMaker<GetUsers>('users/GET_USERS');

export type GetUsersResult = FailableAction<
  'users/GET_USERS_RESULT',
  { count: number; data: IUser[] }
>;

export const [getUsersOk, getUsersErr] = failableActionMaker<GetUsersResult>(
  'users/GET_USERS_RESULT'
);

export interface IDeleteUser {
  type: 'userdelete/DELETE_USER';
  payload: string;
}

export const deleteUser = (login: string): IDeleteUser => ({
  type: 'userdelete/DELETE_USER',
  payload: login,
});

export type DeleteUserResult = FailableAction<
  'userdelete/DELETE_USER_RESULT',
  undefined
>;
export const [deleteUserOk, deleteUserErr] = bothWithMeta(
  { successMessage: 'User deleted', errorMessage: 'Error deleting user' },
  failableActionMaker<DeleteUserResult>('userdelete/DELETE_USER_RESULT')
);

export type UsersAction =
  | GetUsers
  | GetUsersResult
  | IDeleteUser
  | DeleteUserResult
  | LocationChangeAction;
