import * as React from 'react';
import { IState } from '../store';
import Table, { PaginationConfig, ColumnProps } from 'antd/lib/table';
import { getUsersAction, IUser, deleteUser } from './types';
import { connect } from 'react-redux';
import { Tag, Button, Modal } from 'antd';
import moment from 'moment';
import { push } from 'connected-react-router';
import styled from 'styled-components';
import { absolutePath, appRoutes } from '../home/types';
import UserCreate from '../userdetails/UserCreate';
import UserEdit from '../userdetails/UserEdit';
import UserDetails from '../userdetails/UserDetails';

const confirmDelete = (handleDelete: () => void, user: IUser) => () =>
  Modal.confirm({
    title: 'Delete user',
    content: `Are you sure you want to delete user ${user.login}?`,
    onOk: () => {
      handleDelete();
    },
    onCancel: () => {},
  });

const timeFmt = 'DD/MM/YYYY';
const formatTime = (t: any) => {
  const time = moment(t);
  if (!time.isValid()) {
    return '';
  }
  return time.format(timeFmt);
};

const columns = (
  viewUser: (_: IUser) => () => void,
  editUser: (_: IUser) => () => void,
  handleDelete: (_: IUser) => () => void,
  account: string
): ColumnProps<IUser>[] => [
  //{ dataIndex: 'id', title: 'Id', key: 'id', width: '20px' },
  { dataIndex: 'login', title: 'Login', key: 'login', fixed: true },
  //{ dataIndex: 'firstName', title: 'First Name', key: 'firstName' },
  //{ dataIndex: 'lastName', title: 'Last Name', key: 'lastName' },
  { dataIndex: 'email', title: 'Email', key: 'lastName' },
  //{ dataIndex: 'imageUrl', title: 'imageUrl', key: 'imageUrl' },
  {
    dataIndex: 'activated',
    title: 'Activated',
    key: 'activated',
    render: (_: string, prf: IUser) =>
      prf.activated ? (
        <Tag color="#87d068">Activated</Tag>
      ) : (
        <Tag color="#FF0000">Deactivated</Tag>
      ),
  },
  {
    dataIndex: 'authorities',
    title: 'Authorities',
    key: 'authorities',
    render: (_: string, prf: IUser) =>
      (prf.authorities || []).map(auth => (
        <Tag key={auth} color="blue" children={auth} />
      )),
  },
  //{ dataIndex: 'langKey', title: 'LangKey', key: 'langKey' },
  { dataIndex: 'createdBy', title: 'Created By', key: 'createdBy' },
  {
    dataIndex: 'createdDate',
    title: 'Created',
    key: 'createdDate',
    render: formatTime,
  },
  {
    dataIndex: 'lastModifiedBy',
    title: 'Last Modified By',
    key: 'lastModifiedBy',
  },
  {
    dataIndex: 'lastModifiedDate',
    title: 'Last Modified',
    key: 'lastModifiedDate',
    render: formatTime,
  },
  {
    title: 'Actions',
    key: 'actions',
    render: (_: string, prf: IUser) => (
      <Button.Group>
        <Button onClick={viewUser(prf)} type="primary" ghost icon="eye" />
        <Button onClick={editUser(prf)} type="primary" ghost icon="edit" />
        <Button
          onClick={confirmDelete(handleDelete(prf), prf)}
          ghost
          icon="delete"
          disabled={prf.login === account}
          type="danger"
        />
      </Button.Group>
    ),
  },
];

type Props = ReturnType<typeof mapStateToProps> &
  ReturnType<typeof mapDispatchToProps>;

const Flexed = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  margin: 8px 0;
`;
const UserManagement = ({
  users,
  err,
  loading,
  page,
  size,
  itemsPerPage,
  handleDelete,
  viewUser,
  editUser,
  goToCreateUser,
  account,
}: Props) => (
  <div>
    <UserCreate />
    <UserEdit />
    <UserDetails />
    <Flexed>
      <Button type="primary" ghost icon="user-add" onClick={goToCreateUser}>
        Create new user
      </Button>
    </Flexed>
    {!!err ? (
      'Whoops'
    ) : (
      <Table
        scroll={{ x: true }}
        dataSource={users.map(prf => ({ ...prf, key: prf.id }))}
        loading={loading}
        columns={columns(
          viewUser,
          editUser,
          handleDelete,
          account.map(u => u.login).withDefault('')
        )}
        pagination={{ current: page, total: size, pageSize: itemsPerPage }}
      />
    )}
  </div>
);

const mapStateToProps = ({ users, signin }: IState) => ({
  users: users.users.withDefault(users.memo),
  loading: users.users.caseOf({ loading: () => true, _: () => false }),
  err: users.users.caseOf({ err: e => e, _: () => undefined }),
  page: users.query.page + 1,
  itemsPerPage: users.query.itemsPerPage,
  size: users.count,
  account: signin.account,
});

const mapDispatchToProps = (dispatch: React.Dispatch<any>) => ({
  goToCreateUser: () =>
    dispatch(push(absolutePath(appRoutes)('Users management', 'Create User'))),
  viewUser: ({ login }: IUser) => () =>
    dispatch(push(`/users-management/details/${login}`)),
  editUser: ({ login }: IUser) => () =>
    dispatch(push(`/users-management/edit/${login}`)),
  handleDelete: ({ login }: IUser) => () => dispatch(deleteUser(login)),
  onChangeHandler: ({ current, pageSize }: PaginationConfig) =>
    dispatch(
      getUsersAction({
        page: !!current ? current - 1 : undefined,
        itemsPerPage: pageSize,
      })
    ),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserManagement);
