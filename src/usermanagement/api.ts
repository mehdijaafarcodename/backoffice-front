import Axios from 'axios';
import { extractPaginatedData } from '../utils';
import { isUser } from './types';

export const getUsers = (page = 0, size = 20) =>
  Axios.get('/uaa/api/users/', { params: { page, size } }).then(
    extractPaginatedData(isUser)
  );
