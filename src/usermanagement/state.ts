import {
  NotRequested,
  Loading,
  makeLenses,
  makeReducer_,
  Unpack,
  Result,
} from '../utils';
import {
  IUsersState,
  UsersAction,
  getUsersAction,
  IDeleteUser,
  deleteUserOk,
  deleteUserErr,
  GetUsers,
  getUsersOk,
  getUsersErr,
  DeleteUserResult,
} from './types';
import { deleteUser } from '../userdetails/api';
import { IState } from '../store';
import * as api from './api';
import { all, call, put, select } from 'redux-saga/effects';
import { LocationChangeAction, LOCATION_CHANGE } from 'connected-react-router';
import { matchPath } from 'react-router';
import { absolutePath, appRoutes } from '../home/types';
import { IUpdateUserResult, ICreateUserResult } from '../userdetails/types';
import { takeLatestAction } from '../utils/saga';

export const usersInitialState: IUsersState = {
  memo: [],
  users: NotRequested(),
  query: { itemsPerPage: 0, page: 0 },
  count: 0,
  deleteResult: NotRequested(),
};

const _l = makeLenses(usersInitialState);

export const usersReducer = makeReducer_<UsersAction, IUsersState>(
  usersInitialState,
  {
    'users/GET_USERS': p => [
      _l.query.modify(q => ({ ...q, p })),
      _l.users.set(Loading()),
      _l.deleteResult.set(NotRequested()),
    ],
    'users/GET_USERS_RESULT': p => [
      _l.users.set(p.map(res => res.data)),
      _l.memo.set(p.map(res => res.data).withDefault([])),
      _l.count.set(p.map(res => res.count).withDefault(0)),
    ],
    'userdelete/DELETE_USER': () => _l.deleteResult.set(Loading()),
    'userdelete/DELETE_USER_RESULT': p =>
      _l.deleteResult.set(p.map(() => undefined)),
    '@@router/LOCATION_CHANGE': p =>
      !matchPath(p.location.pathname, appRoutes['Users management'].path)
        ? _l.users.set(NotRequested())
        : s => s,
  }
);

function* getUsersEffect(_: GetUsers) {
  try {
    const { page, itemsPerPage }: IUsersState['query'] = yield select(
      (s: IState) => s.users.query
    );
    const data: Unpack<typeof api.getUsers> = yield call(
      api.getUsers,
      page,
      itemsPerPage
    );
    yield put(getUsersOk(data));
  } catch (e) {
    yield put(getUsersErr(e));
  }
}

function* deleteUserEffect(a: IDeleteUser) {
  try {
    yield call(deleteUser, a.payload);
    yield put(deleteUserOk(undefined));
    yield put(getUsersAction({}));
  } catch (e) {
    yield put(deleteUserErr(e));
  }
}

function* changePageEffect(a: LocationChangeAction) {
  const match = matchPath(a.payload.location.pathname, {
    path: absolutePath(appRoutes)('Users management'),
  });
  const hasUsers: IUsersState['users'] = yield select((s: IState) =>
    s.users.users.map(() => true).withDefault(false)
  );
  if (!!match && !hasUsers) {
    yield put(getUsersAction({}));
  }
}

function* refreshUsers(
  a: DeleteUserResult | IUpdateUserResult | ICreateUserResult
) {
  const p: Result<any> = a.payload;
  const successful = p.map(() => true).withDefault(false);
  if (successful) {
    yield put(getUsersAction({}));
  }
}

export function* usersEffects() {
  yield all([
    takeLatestAction(LOCATION_CHANGE, changePageEffect),
    takeLatestAction('users/GET_USERS', getUsersEffect),
    takeLatestAction<DeleteUserResult, IUpdateUserResult, ICreateUserResult>(
      [
        'userdelete/DELETE_USER_RESULT',
        'userupdate/UPDATE_USER_RESULT',
        'usercreate/CREATE_USER_RESULT',
      ],
      refreshUsers
    ),
    takeLatestAction('userdelete/DELETE_USER', deleteUserEffect),
  ]);
}
