import { implementsInterface, isArrayOf } from './type-assertion';
import { isString, isUndefined, isNumber, isSymbol } from 'util';
import { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';
import { function as FN } from 'fp-ts';

export type FieldErrorType = 'Size' | 'NotBlank' | 'Email';

export const isFieldErrorType = (x: unknown): x is FieldErrorType =>
  x === 'Size' || x === 'NotBlank' || x === 'Email';

export interface IFieldError<T extends object> {
  field: keyof T;
  message: FieldErrorType;
}

export const fieldErrorToMessage = <T extends object = object>({
  field,
  message,
}: IFieldError<T>) => {
  const fStr = isSymbol(field) ? field.toString() : field;
  switch (message) {
    case 'Size':
      return `${fStr} has an incorrect size`;
    case 'Email':
      return `${fStr} has an invalid format`;
    case 'NotBlank':
      return `${fStr} cannot be empty`;
    default:
      return `${fStr} is invalid`;
  }
};

export const isFieldError = implementsInterface<IFieldError<any>>({
  field: isString,
  message: isFieldErrorType,
});

export interface IValidationError<T extends object> {
  message: 'error.validation';
  fieldErrors: IFieldError<T>[];
}

export const isValidationError = implementsInterface<IValidationError<any>>({
  message: (x: unknown): x is 'error.validation' => x === 'error.validation',
  fieldErrors: isArrayOf(isFieldError),
});

export interface IUserExistsError {
  message: 'error.userexists';
}

export const isUserExistsError = implementsInterface<IUserExistsError>({
  message: (x: unknown): x is 'error.userexists' => x === 'error.userexists',
});

export interface IEmailExistsError {
  message: 'error.emailexists';
}

export const isEmailExistsError = implementsInterface<IEmailExistsError>({
  message: (x: unknown): x is 'error.emailexists' => x === 'error.emailexists',
});

export const isAxiosResponse = implementsInterface<AxiosResponse<any>>({
  data: (_x: unknown): _x is any => true,
  status: isNumber,
  statusText: isString,
  headers: (_x: unknown): _x is any => true,
  config: (_x: unknown): _x is any => true,
  request: (_x: unknown): _x is any => true,
});

export const isAxiosError = implementsInterface<AxiosError>({
  message: isString,
  name: isString,
  config: (_x: unknown): _x is AxiosRequestConfig => true,
  code: FN.or(isString, isUndefined),
  request: (_x: unknown): _x is any => true,
  response: isAxiosResponse,
  stack: FN.or(isString, isUndefined),
});
