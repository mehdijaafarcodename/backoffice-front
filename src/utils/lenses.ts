import { Lens } from 'monocle-ts';
import { Loadable, isLoadable } from './loadable';
import { Result, isResult } from './result';
import { record as REC } from 'fp-ts';

type LensesOfObj<Src extends object, T extends object> = {
  [key in keyof T]: Lens<Src, T[key]>
};

export const makeLenses = <T extends object>(
  o: T
): {
  [key in keyof T]: T[key] extends Loadable<any>
    ? Lens<T, T[key]>
    : T[key] extends Result<any>
    ? Lens<T, T[key]>
    : T[key] extends object
    ? LensesOfObj<T, T[key]> & Lens<T, T[key]>
    : Lens<T, T[key]>
} =>
  // @ts-ignore
  REC.mapWithKey(o, (k, v) => {
    const baseLens = Lens.fromProp(k);
    return typeof v === 'object' && !!v && !isLoadable(v) && !isResult(v)
      ? // @ts-ignore
        {
          ...REC.map(makeLenses(v), baseLens.compose.bind(baseLens)),
          ...baseLens,
          modify: baseLens.modify,
        }
      : baseLens;
  });
