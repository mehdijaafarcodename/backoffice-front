import { implementsInterface } from './type-assertion';
import { ResultTypes, isResultType } from './result';
import { function as FN } from 'fp-ts';

type LoadableCaseOf<T, X> =
  | {
      ok: (_: T) => X;
      err: (_: Error) => X;
      loading: () => X;
      notRequested: () => X;
    }
  | {
      ok?: (_: T) => X;
      err?: (_: Error) => X;
      loading?: () => X;
      notRequested?: () => X;
      _: () => X;
    };

const loadingSym = Symbol('loading');
const notRequestedSym = Symbol('not requested');

export type LoadableTypes =
  | ResultTypes
  | typeof loadingSym
  | typeof notRequestedSym;

const isLoadableType = (x: unknown): x is LoadableTypes =>
  FN.or(isResultType, x => x === loadingSym || x === notRequestedSym)(x);

export type Loadable<T> = {
  type: LoadableTypes;
  map: <X>(_: (_: T) => X) => Loadable<X>;
  withDefault: (_: T) => T;
  caseOf: <X>(_: LoadableCaseOf<T, X>) => X;
};

export const Loading = <T>(): Loadable<T> => ({
  type: loadingSym,
  map: <X>(_f: (_: T) => X) => Loading<X>(),
  withDefault: (def: T) => def,
  caseOf: <X>(fns: LoadableCaseOf<T, X>) =>
    !!fns.loading ? fns.loading() : fns._(),
});

export const NotRequested = <T>(): Loadable<T> => ({
  type: notRequestedSym,
  map: <X>(_f: (_: T) => X) => NotRequested<X>(),
  withDefault: (def: T) => def,
  caseOf: <X>(fns: LoadableCaseOf<T, X>) =>
    !!fns.notRequested ? fns.notRequested() : fns._(),
});

// isLoadable returns true if argument is of type Loadable<unknown>. You should
// be careful when using this function as it is type safe as it only asserts on the
// existence of the type parameter and the the value of that type parameter
export const isLoadable = implementsInterface<{ type: LoadableTypes }>({
  type: isLoadableType,
}) as (x: unknown) => x is Loadable<unknown>;
