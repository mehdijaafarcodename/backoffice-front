type _U<T> = T extends (...args: any[]) => infer R
  ? R
  : T extends (infer A)[]
  ? A
  : T extends Promise<infer P>
  ? P
  : T extends IterableIterator<infer I>
  ? I
  : T;

export type Unpack<T> = _U<_U<_U<_U<_U<_U<_U<_U<_U<T>>>>>>>>>;
