import {
  TypeOfAction,
  FailableAction,
  PayloadAction,
  Action,
  isPayloadAction,
} from './actions';
import { isArray } from 'util';
import { function as FN } from 'fp-ts';

type ActionCaseOf<A extends Action<string>, S> = {
  [key in TypeOfAction<A>]: (
    a: A extends FailableAction<key, unknown>
      ? A
      : A extends PayloadAction<key, unknown>
      ? A
      : A extends Action<key>
      ? A
      : never,
    s: S
  ) => S
};
export const makeReducer = <A extends Action<string>, S>(
  init: S,
  cases: ActionCaseOf<A, S>
) => (s: S | undefined, a: A): S => {
  const state = s || init;
  // @ts-ignore
  return !!cases[a.type] ? cases[a.type](a, state) : state;
};

type ActionCaseOf_<Keys extends string, A extends Action<Keys>, S> = {
  [key in Keys]: (
    p: A extends PayloadAction<key, infer T>
      ? T
      : A extends Action<key>
      ? undefined
      : never
  ) => ((s: S) => S) | ((s: S) => S)[]
};

export const makeReducer_ = <A extends Action<string>, S>(
  init: S,
  cases: ActionCaseOf_<A['type'], A, S>
) => (s: S | undefined, a: A): S => {
  const state = s || init;
  const handler: ((_: A) => ((_: S) => S) | ((_: S) => S)[]) | undefined =
    // @ts-ignore
    cases[a.type];
  if (!handler) {
    return state;
  }
  const stateTransformer = handler(isPayloadAction(a) ? a.payload : undefined);
  if (isArray(stateTransformer)) {
    // @ts-ignore
    return FN.compose(...stateTransformer)(state);
  }
  return stateTransformer(state);
};
