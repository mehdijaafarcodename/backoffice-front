import { implementsInterface } from './type-assertion';

type ResultCaseOf<T, X> =
  | {
      ok: (_: T) => X;
      err: (_: Error) => X;
    }
  | {
      ok?: (_: T) => X;
      err?: (_: Error) => X;
      _: () => X;
    };

const okSym = Symbol('ok');
const errSym = Symbol('error');

export type ResultTypes = typeof okSym | typeof errSym;

export const isResultType = (x: unknown): x is ResultTypes =>
  x === okSym || x === errSym;

export type Result<T> = {
  type: ResultTypes;
  val?: T | Error;
  map: <X>(_: (_: T) => X) => Result<X>;
  withDefault: (_: T) => T;
  caseOf: <X>(_: ResultCaseOf<T, X>) => X;
};

// isResult returns true if argument is of type Result<unknown>. You should be
// careful when using this function as it is type safe as it only asserts on the
// existence of the type parameter and the the value of that type parameter
export const isResult = implementsInterface<{ type: ResultTypes }>({
  type: isResultType,
}) as (x: unknown) => x is Result<unknown>;

export const Ok = <T>(x: T): Result<T> => ({
  type: okSym,
  val: x,
  map: <X>(f: (_: T) => X) => Ok(f(x)),
  withDefault: (_: T) => x,
  caseOf: <X>(fns: ResultCaseOf<T, X>) => (!!fns.ok ? fns.ok(x) : fns._()),
});

export const Err = <T>(e: Error): Result<T> => ({
  type: errSym,
  val: e,
  map: <X>(_f: (_: T) => X) => Err<X>(e),
  withDefault: (def: T) => def,
  caseOf: <X>(fns: ResultCaseOf<T, X>) => (!!fns.err ? fns.err(e) : fns._()),
});
