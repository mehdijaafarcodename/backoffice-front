import { isArray } from 'util';

export type TypeAsserter<T> = (x: unknown) => x is T;
export const implementsInterface = <T extends { [_: string]: unknown }>(
  predicates: { [Key in keyof T]: (x: unknown) => x is T[Key] }
) => (x: unknown): x is T => {
  if (typeof x !== 'object' || !x) {
    return false;
  }
  const keys = Object.keys(predicates) as (keyof T)[];
  for (const k of keys) {
    // @ts-ignore
    if (!predicates[k](x[k])) {
      return false;
    }
  }
  return true;
};

export const isArrayOf = <T>(predicate: (x: unknown) => x is T) => (
  x: unknown
): x is T[] => isArray(x) && !x.some(y => !predicate(y));
