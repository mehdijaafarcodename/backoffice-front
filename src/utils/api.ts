import axios, { AxiosResponse } from 'axios';
import { IProfile } from '../profiles/types';
import { IUser } from '../usermanagement/types';
import { isArrayOf } from '../utils';

export interface IProfilesResult {
  count: number;
  pages: number;
  profiles: IProfile[];
}

export interface IProfileResult {
  count: number;
  pages: number;
  profiles: IProfile[];
}

export interface IUsersResult {
  count: number;
  users: IUser[];
}

export const getProfiles = (page = 0, size = 20) =>
  axios.get<IProfilesResult>('/api/profiles-controller/all', {
    params: { page, size },
  });

export const getUsers = (page = 0, size = 20) =>
  axios.get<IUsersResult>('/uaa/api/users/', {
    params: { page, size },
  });

export const assertType = <T>(fn: (y: unknown) => y is T) => (
  resp: AxiosResponse<unknown>
): AxiosResponse<T> => {
  const { data } = resp;
  if (fn(data)) {
    return { ...resp, data };
  }
  throw new TypeError('unexpected format');
};

export const extractData = <T>(fn: (y: unknown) => y is T) => (
  resp: AxiosResponse<unknown>
): T => {
  const { data } = resp;
  if (fn(data)) {
    return data;
  }
  throw new TypeError('unexpected format');
};

export const extractPaginatedData = <T>(fn: (y: unknown) => y is T) => (
  resp: AxiosResponse<unknown>
): { data: T[]; count: number } => {
  const data = extractData(isArrayOf(fn))(resp);
  return {
    data,
    count: parseInt(resp.headers['x-total-count'], 10),
  };
};

export const splitPath = (s: string) => s.split('/').map(s => '/' + s);
