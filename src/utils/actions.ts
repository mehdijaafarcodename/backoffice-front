import { implementsInterface, TypeAsserter } from './type-assertion';
import { isString, isUndefined } from 'util';
import { function as FN } from 'fp-ts';
import { Result, Ok, Err } from './result';
import { isLoadable, Loadable } from './loadable';

export type Action<T extends string> = {
  type: T;
};

export const isAction = implementsInterface<Action<string>>({
  type: isString,
});

export type PayloadAction<T extends string, P> = {
  type: T;
  payload: P;
};
export const isPayloadAction = FN.and(
  isAction,
  implementsInterface<{ payload: unknown }>({
    payload: (p: unknown): p is unknown => !!p,
  })
) as TypeAsserter<PayloadAction<string, any>>;

export type FailableAction<T extends string, P> = PayloadAction<T, Result<P>>;
export const isFailableAction = FN.and(
  isPayloadAction,
  implementsInterface<{ payload: Loadable<unknown> }>({ payload: isLoadable })
) as TypeAsserter<FailableAction<string, unknown>>;

export type TypeOfAction<A extends Action<string>> = A extends Action<infer T>
  ? T
  : never;

export type PayloadOfAction<
  A extends PayloadAction<string, unknown>
> = A extends FailableAction<string, infer T>
  ? T
  : A extends PayloadAction<string, infer T2>
  ? T2
  : never;

export const actionMaker = <A extends Action<string>>(
  type: TypeOfAction<A>
) => (): Action<TypeOfAction<A>> => ({ type });

export const payloadActionMaker = <A extends PayloadAction<string, unknown>>(
  type: TypeOfAction<A>
) => (
  payload: PayloadOfAction<A>
): PayloadAction<TypeOfAction<A>, PayloadOfAction<A>> => ({ type, payload });

export const failableActionMaker = <A extends FailableAction<string, any>>(
  type: TypeOfAction<A>
): [
  (
    _: PayloadOfAction<A>
  ) => FailableAction<TypeOfAction<A>, PayloadOfAction<A>>,
  (err: Error) => FailableAction<TypeOfAction<A>, PayloadOfAction<A>>
] => [
  (payload: PayloadOfAction<A>) => ({ type, payload: Ok(payload) }),
  (e: Error) => ({ type, payload: Err(e) }),
];

export type Meta = {
  successMessage?: string;
  errorMessage?: string;
};

type ArgumentTypes<T extends (...args: unknown[]) => unknown> = T extends (
  ...args: infer U
) => unknown
  ? U
  : never;
type ReplaceReturnType<T extends (...args: unknown[]) => unknown, Ret> = (
  ...a: ArgumentTypes<T>
) => Ret;

export const withMeta = <
  A extends Action<string>,
  T extends (...args: any[]) => A
>(
  meta: Meta,
  f: T
): ReplaceReturnType<T, WithMeta<A>> => (...x: ArgumentTypes<T>) => ({
  ...f(...x),
  meta,
});

export const bothWithMeta = <
  A extends FailableAction<string, any>,
  T1 extends (...args: any[]) => A,
  T2 extends (...args: any[]) => A
>(
  meta: Meta,
  [f1, f2]: [T1, T2]
): [ReplaceReturnType<T1, WithMeta<A>>, ReplaceReturnType<T2, WithMeta<A>>] => [
  withMeta(meta, f1),
  withMeta(meta, f2),
];

export const isMeta = implementsInterface<Meta>({
  successMessage: FN.or(isString, isUndefined),
  errorMessage: FN.or(isString, isUndefined),
});

type WithMeta<A extends Action<string>> = { meta: Meta } & A;

export const hasMeta = (x: unknown): x is WithMeta<Action<string>> =>
  isMeta((x as { meta: unknown }).meta) && isAction(x);
