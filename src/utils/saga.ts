import * as effects from 'redux-saga/effects';
import { Action, TypeOfAction } from './actions';

interface takeLatestActionSignature {
  <
    A1 extends Action<string>,
    A2 extends Action<string>,
    A3 extends Action<string>,
    A4 extends Action<string>
  >(
    type: [
      TypeOfAction<A1>,
      TypeOfAction<A2>,
      TypeOfAction<A3>,
      TypeOfAction<A4>
    ],
    effect: effects.HelperFunc0<A1 | A2 | A3 | A4>
  ): effects.ForkEffect;
  <
    A1 extends Action<string>,
    A2 extends Action<string>,
    A3 extends Action<string>
  >(
    type: [TypeOfAction<A1>, TypeOfAction<A2>, TypeOfAction<A3>],
    effect: effects.HelperFunc0<A1 | A2 | A3>
  ): effects.ForkEffect;
  <A1 extends Action<string>, A2 extends Action<string>>(
    type: [TypeOfAction<A1>, TypeOfAction<A2>],
    effect: effects.HelperFunc0<A1 | A2>
  ): effects.ForkEffect;
  <A extends Action<string>>(
    type: ((x: Action<string>) => x is A),
    effect: effects.HelperFunc0<A>
  ): effects.ForkEffect;
  <A extends Action<string>>(
    type: TypeOfAction<A>,
    effect: effects.HelperFunc0<A>
  ): effects.ForkEffect;
}
export const takeLatestAction: takeLatestActionSignature = (
  type: any,
  effect: any
) => effects.takeLatest(type, effect);
