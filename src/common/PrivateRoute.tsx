import { IState } from '../store';
import { Route, Redirect, RouteProps } from 'react-router';
import { connect } from 'react-redux';
import React from 'react';

const mapStateToProps = ({ signin: { account }, router }: IState) => ({
  roles: account.map(a => a.authorities).withDefault([]),
  isLoggedIn: account.map(() => true).withDefault(false),
  ...router,
});

type StateProps = ReturnType<typeof mapStateToProps>;

type OwnProps = { requiredRole?: string };
const PrivateRoute = ({
  roles,
  isLoggedIn,
  requiredRole,
  ...routeProps
}: StateProps & RouteProps & OwnProps) =>
  isLoggedIn ? (
    !requiredRole || roles.includes(requiredRole) ? (
      <Route {...routeProps} />
    ) : (
      <Redirect to="/" />
    )
  ) : (
    <Redirect to="/login" />
  );

export default connect(mapStateToProps)(PrivateRoute);
