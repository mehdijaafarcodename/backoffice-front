import React from 'react';
import { Button } from 'antd';

export default () => (
  <Button style={{ width: 0, height: 0, display: 'none' }} htmlType="submit" />
);
