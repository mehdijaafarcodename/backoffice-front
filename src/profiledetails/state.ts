import {
  IProfileDetailsState,
  ProfileAction,
  getProfile,
  GetProfile,
  getProfileResultErr,
  getProfileResultOk,
} from './types';
import { NotRequested, Loading, makeReducer_, Unpack } from '../utils';
import { LocationChangeAction, LOCATION_CHANGE } from 'connected-react-router';
import { matchPath } from 'react-router';
import { all, put, call } from 'redux-saga/effects';
import { makeLenses } from '../utils/lenses';
import * as api from './api';
import { absolutePath, appRoutes } from '../home/types';
import { takeLatestAction } from '../utils/saga';

export const profileDetailsInitialState: IProfileDetailsState = {
  profile: NotRequested(),
  selectetContactField: { state: 'closed' },
};

const _l = makeLenses(profileDetailsInitialState);

export const profileDetailsReducer = makeReducer_<
  ProfileAction,
  IProfileDetailsState
>(profileDetailsInitialState, {
  'profiledetails/GET_PROFILE': () => _l.profile.set(Loading()),
  'profiledetails/GET_PROFILE_RESULT': p => _l.profile.set(p),
  'profiledetails/SHOW_PICTURE': p =>
    _l.selectetContactField.set({ contactField: p, state: 'selected' }),
  'profiledetails/CLOSE_MODAL': () =>
    _l.selectetContactField.modify(p =>
      p.state === 'selected' ? { ...p, state: 'closing' } : p
    ),
  'profiledetails/UNSELECT_MODAL': () =>
    _l.selectetContactField.set({ state: 'closed' }),
  '@@router/LOCATION_CHANGE': p => s =>
    !matchPath(p.location.pathname, {
      path: absolutePath(appRoutes)('Profiles', 'Profile Details'),
      exact: true,
    })
      ? profileDetailsInitialState
      : s,
});

function* getProfileEffect(a: GetProfile) {
  try {
    // get profile from the server
    const data: Unpack<typeof api.getProfile> = yield call(
      api.getProfile,
      a.payload
    );
    yield put(getProfileResultOk(data));
  } catch (e) {
    yield put(getProfileResultErr(e));
  }
}

function* changePageEffect(a: LocationChangeAction) {
  const match = matchPath<{ nym: string }>(a.payload.location.pathname, {
    path: absolutePath(appRoutes)('Profiles', 'Profile Details'),
    exact: true,
  });
  if (!!match) {
    yield put(getProfile(match.params.nym));
  }
}

export function* profileDetailsEffects() {
  yield all([
    takeLatestAction(LOCATION_CHANGE, changePageEffect),
    takeLatestAction('profiledetails/GET_PROFILE', getProfileEffect),
  ]);
}
