import Axios from 'axios';
import { extractData } from '../utils';
import { isProfileDetails } from './types';

const apiProfile = '/backoffice/api/profiles';

export const getProfile = (id: string) =>
  Axios.get(`${apiProfile}/${id}`).then(extractData(isProfileDetails));
