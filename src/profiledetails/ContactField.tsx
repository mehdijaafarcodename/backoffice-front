import React from 'react';
import styled from 'styled-components/macro';
import { Select, Card } from 'antd';
import {
  AcceptedProfileFieldKeys,
  isPictureKey,
  profileKeysLookup,
} from './types';
import { CardProps } from 'antd/lib/card';

const defaultPicture = 'TODO: Replace me with a default picture';

interface StyledCardProps {
  fieldKey: AcceptedProfileFieldKeys;
}
const StyledCard = styled(
  ({ fieldKey, ...props }: CardProps & { fieldKey: string }) => (
    <Card {...props} />
  )
)`
  grid-row: span
    ${(props: StyledCardProps) => (isPictureKey(props.fieldKey) ? 2 : 1)};
  maxheight: ${(props: StyledCardProps) =>
    (isPictureKey(props.fieldKey) ? 2 : 1) * 140}px;
  overflow: hidden;
  object-fit: cover;
`;

const CardContent = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const Image = styled.img`
  maxheight: 175;
`;

const StyledSelect = styled(Select)`
  min-width: 120px;
`;

interface IProfileFieldProps {
  fieldKey: AcceptedProfileFieldKeys;
  value?: string;
  setStatus: () => void;
  status: string;
  select?: () => void;
}
export default ({
  fieldKey,
  value,
  status,
  setStatus,
  select,
}: IProfileFieldProps) => (
  <StyledCard
    title={profileKeysLookup[fieldKey]}
    bordered={false}
    fieldKey={fieldKey}
  >
    <CardContent>
      {isPictureKey(fieldKey) ? (
        <Image src={value || defaultPicture} onClick={select} />
      ) : (
        <h2>{value}</h2>
      )}
      <StyledSelect onSelect={setStatus} defaultValue={status}>
        <Select.Option value="Approve">Approve</Select.Option>
        <Select.Option value="Disapprove">Disapprove</Select.Option>
      </StyledSelect>
    </CardContent>
  </StyledCard>
);
