import {
  Loadable,
  PayloadAction,
  payloadActionMaker,
  Action,
  actionMaker,
  FailableAction,
  failableActionMaker,
  implementsInterface,
  isArrayOf,
} from '../utils';
import { LocationChangeAction } from 'connected-react-router';
import { record as REC, function as FN, array as AR } from 'fp-ts';
import { isString } from 'util';

export interface IProfileDetailsContact {
  fieldName: string;
  value: string;
  status: string;
  visibility: string;
  createdAt: string;
}

export interface IProfileDetails {
  nymId: string;
  contactFields: IProfileDetailsContact[];
}

export const isProfileDetailsContact = implementsInterface<
  IProfileDetailsContact
>({
  createdAt: isString,
  status: isString,
  fieldName: isString,
  value: isString,
  visibility: isString,
});

export const isProfileDetails = implementsInterface<IProfileDetails>({
  nymId: isString,
  contactFields: isArrayOf(isProfileDetailsContact),
});

export interface IProfileDetailsState {
  profile: Loadable<IProfileDetails>;
  selectetContactField:
    | { contactField: IProfileDetailsContact; state: 'selected' | 'closing' }
    | { state: 'closed' };
}

export type GetProfile = PayloadAction<'profiledetails/GET_PROFILE', string>;

export const getProfile = payloadActionMaker<GetProfile>(
  'profiledetails/GET_PROFILE'
);

export type GetProfileResult = FailableAction<
  'profiledetails/GET_PROFILE_RESULT',
  IProfileDetails
>;
export const [getProfileResultOk, getProfileResultErr] = failableActionMaker<
  GetProfileResult
>('profiledetails/GET_PROFILE_RESULT');

export type SelectPicture = PayloadAction<
  'profiledetails/SHOW_PICTURE',
  IProfileDetailsContact
>;
export const selectPicture = payloadActionMaker<SelectPicture>(
  'profiledetails/SHOW_PICTURE'
);

export type CloseModal = Action<'profiledetails/CLOSE_MODAL'>;
export const closeModal = actionMaker<CloseModal>('profiledetails/CLOSE_MODAL');

export type UnselectContactField = Action<'profiledetails/UNSELECT_MODAL'>;
export const unselectContactField = actionMaker<UnselectContactField>(
  'profiledetails/UNSELECT_MODAL'
);

export type ProfileAction =
  | GetProfile
  | GetProfileResult
  | SelectPicture
  | CloseModal
  | UnselectContactField
  | LocationChangeAction;

export type PictureKeys = '_cin_recto' | '_cin_verso' | '_user_face';

const pictureKeys: { [key in PictureKeys]: true } = {
  _cin_recto: true,
  _cin_verso: true,
  _user_face: true,
};

export const isPictureKey = (x: unknown): x is PictureKeys =>
  typeof x === 'string' && !!REC.lookup(x, pictureKeys).toUndefined();

export type AcceptedProfileFieldKeys =
  | PictureKeys
  | '_first_name'
  | '_last_name'
  | '_cin'
  | '_scanned_cin'
  | '_phonenumber'
  | '_user_gender'
  | '_user_governorate'
  | '_user_birth_date'
  | '_emailaddress';

const acceptedProfileFieldKeys: { [key in AcceptedProfileFieldKeys]: true } = {
  ...pictureKeys,
  _cin: true,
  _cin_recto: true,
  _cin_verso: true,
  _emailaddress: true,
  _first_name: true,
  _user_face: true,
  _last_name: true,
  _phonenumber: true,
  _scanned_cin: true,
  _user_birth_date: true,
  _user_gender: true,
  _user_governorate: true,
};

export const isAcceptedProfileFieldKeys = (
  x: unknown
): x is AcceptedProfileFieldKeys =>
  typeof x === 'string' &&
  !!REC.lookup(x, acceptedProfileFieldKeys).toUndefined();

export const profileKeysLookup: {
  [key in AcceptedProfileFieldKeys]: string
} = {
  _cin: 'CIN',
  _cin_recto: 'CIN Back',
  _cin_verso: 'CIN Face',
  _emailaddress: 'Email Address',
  _first_name: 'Firstname',
  _last_name: 'Lastname',
  _phonenumber: 'Phone Number',
  _scanned_cin: 'Scanned CIN',
  _user_birth_date: 'Date of Birth',
  _user_face: 'User Picture',
  _user_gender: 'Gender',
  _user_governorate: 'Governorate',
};

export const recordByFieldName = <T extends { fieldName: string }>(
  xs: T[]
): { [Key in AcceptedProfileFieldKeys]?: T } =>
  AR.filter(
    xs.map(x => FN.tuple(x.fieldName, x)),
    (x): x is [AcceptedProfileFieldKeys, T] => isAcceptedProfileFieldKeys(x[0])
  ).reduce(
    (p, [fieldName, contactField]) => REC.insert(fieldName, contactField, p),
    REC.empty as Record<AcceptedProfileFieldKeys, T>
  );
