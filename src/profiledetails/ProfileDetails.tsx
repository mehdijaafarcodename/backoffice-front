import * as React from 'react';
import { Skeleton, Modal } from 'antd';
import { IState } from '../store';
import { connect } from 'react-redux';
import {
  IProfileDetails,
  selectPicture,
  IProfileDetailsContact,
  closeModal,
  unselectContactField,
  AcceptedProfileFieldKeys,
  isPictureKey,
  profileKeysLookup,
  recordByFieldName,
} from './types';
import { function as FN } from 'fp-ts';
import styled from 'styled-components/macro';
import ContactField from './ContactField';

// 20190116181852
// http://192.168.1.4:8084/api/profiles-controller/profile/3fZAZ6EKYJ8eVPUXSfQVeRVZQV9zHw3rhDg5CMkTJsKB

// Ordering of cards in view set by this array.
const profileKeys: AcceptedProfileFieldKeys[] = [
  '_user_face',
  '_cin_recto',
  '_cin_verso',
  '_first_name',
  '_last_name',
  '_cin',
  '_scanned_cin',
  '_user_birth_date',
  '_phonenumber',
  '_user_gender',
  '_user_governorate',
  '_emailaddress',
];

const profileToCard = (select?: (_: IProfileDetailsContact) => void) => ({
  contactFields: cfs,
}: IProfileDetails) => {
  const fields = recordByFieldName(cfs);
  return profileKeys.map(k => {
    const cf = fields[k];
    const value = !!cf ? cf.value : undefined;
    const selecter = !!select && !!cf ? () => select(cf) : undefined;
    return (
      <ContactField
        key={k}
        fieldKey={k}
        setStatus={FN.constNull}
        select={selecter}
        value={value}
        status=""
      />
    );
  });
};

const ProfileDetailGrid = styled.div`
  display: grid;
  grid-gap: 8px;
  grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
`;
const ProfileDetails = ({
  profile,
  selectetContactField,
  closeModal,
  unselectContactField,
  selectPicture,
}: StateProps & DispatchProps) => (
  <>
    <h3>nymId: {profile.map(t => t.nymId).withDefault('')}</h3>
    <ProfileDetailGrid>
      {profile.map(profileToCard(selectPicture)).withDefault([<Skeleton />])}
    </ProfileDetailGrid>
    <Modal
      visible={selectetContactField.state === 'selected'}
      afterClose={unselectContactField}
      width="fit-content"
      closable
      onCancel={closeModal}
      footer={[]}
      title={
        selectetContactField.state !== 'closed' &&
        isPictureKey(selectetContactField.contactField.fieldName)
          ? profileKeysLookup[selectetContactField.contactField.fieldName]
          : ''
      }
    >
      <img
        src={
          selectetContactField.state !== 'closed'
            ? selectetContactField.contactField.value
            : ''
        }
      />
    </Modal>
    <div />
  </>
);

const mapStateToProps = ({ profileDetails }: IState) => profileDetails;
const mapDispatchToProps = { selectPicture, closeModal, unselectContactField };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileDetails);
