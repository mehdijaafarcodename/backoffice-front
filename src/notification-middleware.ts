import { Dispatch } from 'redux';
import { Action, isFailableAction, hasMeta } from './utils';
import { message } from 'antd';
import {
  isAxiosError,
  isAxiosResponse,
  isValidationError,
  fieldErrorToMessage,
  isUserExistsError,
  isEmailExistsError,
} from './utils/errors';

export default () => (next: Dispatch) => (
  action: Action<string>
): Action<string> => {
  if (isFailableAction(action) && hasMeta(action)) {
    action.payload.caseOf({
      ok: () => {
        if (!!action.meta.successMessage) {
          message.info(action.meta.successMessage);
        }
      },
      err: e => {
        if (!!action.meta.errorMessage) {
          message.error(action.meta.errorMessage);
        }
        if (isAxiosError(e)) {
          const { response } = e;
          if (isAxiosResponse(response)) {
            const { data } = response;
            if (isValidationError(data)) {
              data.fieldErrors.forEach(x =>
                message.error(fieldErrorToMessage(x))
              );
            } else if (isUserExistsError(data)) {
              message.error('User already exists');
            } else if (isEmailExistsError(data)) {
              message.error('Email already in use');
            }
          }
        }
      },
    });
  }
  return next(action);
};
