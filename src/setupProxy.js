// @ts-ignore
const proxy = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(
    proxy(['/uaa', '/api', '/dbapi', '/auth', '/backoffice'], {
      target: 'https://rocket-dev.digitus.me:4005',
      secure: false,
      changeOrigin: true,
      headers: { host: 'localhost:3000' },
      cookieDomainRewrite: { '*': 'localhost' },
    })
  );
};
