import Axios from 'axios';
import { IProfilesState } from './profiles/types';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import createSagaMiddleware, { effects } from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createBrowserHistory } from 'history';
import {
  routerMiddleware,
  RouterState,
  connectRouter,
} from 'connected-react-router';
import { IProfileDetailsState } from './profiledetails/types';
import {
  profileDetailsReducer,
  profileDetailsEffects,
} from './profiledetails/state';
import { ISignInState, getSession } from './authentication/login/types';
import { signinEffects, signInReducer } from './authentication/login/state';
import { profilesEffects, profilesReducer } from './profiles/state';
import { IUsersState } from './usermanagement/types';
import { usersReducer, usersEffects } from './usermanagement/state';
import { IUserDetailsState } from './userdetails/types';
import { userReducer, UserEffects } from './userdetails/state';
import notificationMiddleware from './notification-middleware';

Axios.defaults.withCredentials = true;
function* initSaga() {
  yield effects.put(getSession());
}

function* rootSaga() {
  yield effects.fork(initSaga);
  yield effects.all([
    profilesEffects(),
    profileDetailsEffects(),
    signinEffects(),
    usersEffects(),
    UserEffects(),
  ]);
}

const sagaMiddleware = createSagaMiddleware();
export const history = createBrowserHistory();

export interface IState {
  profiles: IProfilesState;
  profileDetails: IProfileDetailsState;
  signin: ISignInState;
  users: IUsersState;
  userDetails: IUserDetailsState;
  router: RouterState;
}

const store = createStore(
  combineReducers({
    signin: signInReducer,
    profiles: profilesReducer,
    profileDetails: profileDetailsReducer,
    users: usersReducer,
    userDetails: userReducer,
    router: connectRouter(history),
  }),
  undefined,
  composeWithDevTools(
    applyMiddleware(
      sagaMiddleware,
      routerMiddleware(history),
      notificationMiddleware
    )
  )
);

sagaMiddleware.run(rootSaga);

export default store;
