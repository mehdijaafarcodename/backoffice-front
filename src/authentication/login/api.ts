import Axios from 'axios';
import { IToken, IAccount } from './types';
import { implementsInterface, isArrayOf, extractData } from '../../utils';
import { isString, isNumber, isNull, isBoolean } from 'util';
import { function as FN } from 'fp-ts';

const POST_AUTHENTICATION_URL = '/auth/login';

const isToken = implementsInterface<IToken>({
  access_token: isString,
  expires_in: isNumber,
  scope: isString,
  token_type: isString,
});

export const signIn = (user: { username: string; password: string }) =>
  Axios.post(POST_AUTHENTICATION_URL, user).then(extractData(isToken));

const GET_ACCOUNT_URL = '/uaa/api/account';

const isAccount = implementsInterface<IAccount>({
  id: isNumber,
  activated: isBoolean,
  authorities: isArrayOf(isString),
  createdBy: isString,
  createdDate: FN.or(isString, isNull),
  email: isString,
  firstName: isString,
  imageUrl: isString,
  langKey: isString,
  lastModifiedBy: isString,
  lastModifiedDate: FN.or(isString, isNull),
  lastName: isString,
  login: isString,
});

export const getAccount = () =>
  Axios.get(GET_ACCOUNT_URL).then(extractData(isAccount));

export const logout = () => Axios.post('auth/logout', {}).then(() => undefined);
