import * as React from 'react';
import { IState } from '../../store';
import { connect } from 'react-redux';
import { IUser, updateSignInData, login, clearLoginStatus } from './types';
import { Form, Input, Icon, Button, Card, Alert, Spin } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import styled from 'styled-components';
import { Redirect } from 'react-router';

type Props = ReturnType<typeof mapStateToProps> &
  ReturnType<typeof mapDispatchToProps>;

const LoginLayout = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background: #aaa;
`;
const DarkIcon = styled(Icon)`
  color: rgba(0, 0, 0, 0.25);
`;
const RedIcon = styled(Icon)`
  color: rgba(0, 0, 0, 0.25);
`;
const FormContainer = styled(Card)`
  min-width: 400px;
`;
const LoginForm = styled(Form)`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 8px;
`;

const SignIn = Form.create<Props>({
  onValuesChange: (p, _cv, values) => p.onChangeHandler(values),
})(
  ({
    account,
    login,
    signIn,
    clearLogin,
    form: { getFieldDecorator, getFieldsError },
  }: Props & FormComponentProps) =>
    account.caseOf({
      ok: () => <Redirect to="/" />,
      _: () => (
        <LoginLayout>
          <FormContainer>
            <Spin
              spinning={login.caseOf({ loading: () => true, _: () => false })}
            >
              <LoginForm onSubmit={signIn}>
                <h2>Login</h2>
                <Form.Item>
                  {getFieldDecorator('username', {
                    rules: [
                      {
                        required: true,
                        message: 'Please input your username!',
                      },
                    ],
                  })(
                    <Input
                      prefix={<DarkIcon type="user" />}
                      placeholder="Username"
                    />
                  )}
                </Form.Item>
                <Form.Item>
                  {getFieldDecorator('password', {
                    rules: [
                      {
                        required: true,
                        message: 'Please input your Password!',
                      },
                    ],
                  })(
                    <Input
                      prefix={<RedIcon type="lock" />}
                      type="password"
                      placeholder="Password"
                    />
                  )}
                </Form.Item>
                <Button
                  style={{ width: '100%' }}
                  type="primary"
                  htmlType="submit"
                  disabled={Object.values(getFieldsError()).some(v => !!v)}
                >
                  Log in
                </Button>
                {login.caseOf({
                  err: () => (
                    <Alert
                      closable
                      onClose={clearLogin}
                      type="error"
                      message="Login failed. Check your username and password"
                    />
                  ),
                  _: () => null,
                })}
              </LoginForm>
            </Spin>
          </FormContainer>
        </LoginLayout>
      ),
    })
);

const mapStateToProps = ({ signin }: IState) => signin;

const mapDispatchToProps = (dispatch: React.Dispatch<any>) => ({
  onChangeHandler: (user: IUser) => dispatch(updateSignInData(user)),
  signIn: (e: React.FormEvent) => {
    e.preventDefault();
    dispatch(login());
  },
  clearLogin: () => dispatch(clearLoginStatus()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignIn);
