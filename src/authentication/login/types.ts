import {
  Loadable,
  Action,
  actionMaker,
  FailableAction,
  failableActionMaker,
  PayloadAction,
  payloadActionMaker,
} from '../../utils';
import { LocationChangeAction } from 'connected-react-router';

export interface IUser {
  username: string;
  password: string;
}

export interface IToken {
  access_token: string;
  token_type: string;
  expires_in: number;
  scope: string;
}

export const ROLE_ADMIN = 'ROLE_ADMIN';

export interface IAccount {
  id: number;
  login: string;
  firstName: string;
  lastName: string;
  email: string;
  imageUrl: string;
  activated: boolean;
  langKey: string;
  createdBy: string;
  createdDate: string | null;
  lastModifiedBy: string;
  lastModifiedDate: string | null;
  authorities: string[];
}

export interface ISignInState {
  account: Loadable<IAccount>;
  login: Loadable<IToken>;
  user: IUser;
}

export type UpdateSignInData = PayloadAction<
  'auth/UPDATE_SIGN_IN_DATA',
  Partial<{ username: string; password: string }>
>;

export const updateSignInData = payloadActionMaker<UpdateSignInData>(
  'auth/UPDATE_SIGN_IN_DATA'
);

export type Login = Action<'auth/LOGIN'>;
export const login = actionMaker<Login>('auth/LOGIN');

export type LoginResult = FailableAction<'auth/LOGIN_RESULT', IToken>;
export const [loginResultOk, loginResultErr] = failableActionMaker<LoginResult>(
  'auth/LOGIN_RESULT'
);

export type GetSession = Action<'auth/GET_SESSION'>;
export const getSession = actionMaker<GetSession>('auth/GET_SESSION');

export type GetSessionResult = FailableAction<
  'auth/GET_SESSION_RESULT',
  IAccount
>;
export const [getSessionResultOk, getSessionResultErr] = failableActionMaker<
  GetSessionResult
>('auth/GET_SESSION_RESULT');

export type ClearLoginStatus = Action<'auth/CLEAR_LOGIN_STATUS'>;
export const clearLoginStatus = actionMaker<ClearLoginStatus>(
  'auth/CLEAR_LOGIN_STATUS'
);

export type Logout = Action<'auth/LOGOUT'>;
export const logout = actionMaker<Logout>('auth/LOGOUT');

export type LogoutResult = FailableAction<'auth/LOGOUT_RESULT', undefined>;
export const [logoutOk, logoutErr] = failableActionMaker<LogoutResult>(
  'auth/LOGOUT_RESULT'
);

export type AuthActions =
  | ClearLoginStatus
  | Logout
  | LogoutResult
  | Login
  | LoginResult
  | UpdateSignInData
  | GetSession
  | GetSessionResult
  | LocationChangeAction;
