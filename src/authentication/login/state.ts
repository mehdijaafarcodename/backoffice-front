import * as api from './api';
import { all, put, call, select } from 'redux-saga/effects';
import { IState } from '../../store';
import {
  ISignInState,
  AuthActions,
  GetSession,
  getSessionResultErr,
  getSessionResultOk,
  getSession,
  Login,
  loginResultOk,
  loginResultErr,
  Logout,
  logoutErr,
  logoutOk,
} from './types';
import {
  NotRequested,
  Loading,
  makeLenses,
  makeReducer_,
  Unpack,
} from '../../utils';
import { matchPath } from 'react-router';
import { takeLatestAction } from '../../utils/saga';

export const signInInitialState: ISignInState = {
  account: NotRequested(),
  login: NotRequested(),
  user: { username: '', password: '' },
};

const _l = makeLenses(signInInitialState);

export const signInReducer = makeReducer_<AuthActions, ISignInState>(
  signInInitialState,
  {
    'auth/UPDATE_SIGN_IN_DATA': p => _l.user.modify(u => ({ ...u, ...p })),
    'auth/LOGIN': () => _l.login.set(Loading()),
    'auth/LOGIN_RESULT': _l.login.set,
    'auth/CLEAR_LOGIN_STATUS': () => _l.login.set(NotRequested()),
    'auth/GET_SESSION': () => _l.account.set(Loading()),
    'auth/GET_SESSION_RESULT': _l.account.set,
    'auth/LOGOUT': () => _l.login.set(Loading()),
    'auth/LOGOUT_RESULT': () => _l.login.set(NotRequested()),
    '@@router/LOCATION_CHANGE': p =>
      !matchPath(p.location.pathname, '/login')
        ? _l.user.set(signInInitialState.user)
        : s => s,
  }
);

function* logoutSaga(_: Logout) {
  try {
    yield call(api.logout);
    yield put(logoutOk(undefined));
  } catch (e) {
    yield put(logoutErr(e));
  } finally {
    yield put(getSession());
  }
}
function* loginSaga(_: Login) {
  try {
    const user: ISignInState['user'] = yield select(
      (s: IState) => s.signin.user
    );
    const data: Unpack<typeof api.signIn> = yield call(api.signIn, user);
    yield put(loginResultOk(data));
    yield put(getSession());
  } catch (e) {
    yield put(loginResultErr(e));
  }
}

export function* getSessionSaga(_: GetSession) {
  try {
    const account: Unpack<typeof api.getAccount> = yield call(api.getAccount);
    yield put(getSessionResultOk(account));
  } catch (e) {
    yield put(getSessionResultErr(e));
  }
}

export function* signinEffects() {
  yield all([
    takeLatestAction('auth/LOGIN', loginSaga),
    takeLatestAction('auth/GET_SESSION', getSessionSaga),
    takeLatestAction('auth/LOGOUT', logoutSaga),
  ]);
}
