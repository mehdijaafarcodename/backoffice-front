import * as React from 'react';
import { IProfile, getProfilesAction } from './types';
import { IState } from '../store';
import { connect } from 'react-redux';
import { Button, Table, Input } from 'antd';
import { PaginationConfig, ColumnProps } from 'antd/lib/table';
import { appRoutes } from '../home/types';
import { push } from 'connected-react-router';

const Search = Input.Search;

const columns = (
  goToDetails: (_: IProfile) => () => void
): (ColumnProps<IProfile> & {
  dataIndex?: keyof IProfile;
})[] => [
  { dataIndex: 'nym', title: 'Nym', key: 'nym', width: '20px', fixed: true },
  { dataIndex: 'firstName', title: 'First Name', key: 'firstName' },
  { dataIndex: 'lastName', title: 'Last Name', key: 'lastName' },
  //{ dataIndex: 'appCategory', title: 'App Category', key: 'appCategory' },
  //{ dataIndex: 'status', title: 'Status', key: 'status' },
  //{ dataIndex: 'scannedCin', title: 'Scanned CIN', key: 'scannedCin' },
  //   { dataIndex: 'cinRecto', title: 'CIN Recto', key: 'cinRecto' },
  //   { dataIndex: 'cinVerso', title: 'CIN Verso', key: 'cinVerso' },
  //   { dataIndex: 'userFace', title: 'User Face', key: 'userFace' },
  //{ dataIndex: 'gender', title: 'Gender', key: 'gender' },
  { dataIndex: 'phoneNumber', title: 'Phone Number', key: 'phoneNumber' },
  //{ dataIndex: 'birthDate', title: 'Birth Date', key: 'birthDate' },
  //{ dataIndex: 'governorate', title: 'Governorate', key: 'governorate' },
  //{ dataIndex: 'imei', title: 'IMEI', key: 'imei' },
  //{dataIndex: 'androidVersion', title: 'Android Version', key: 'androidVersion',},
  //{ dataIndex: 'operatorName', title: 'Operator Name', key: 'operatorName' },
  //{ dataIndex: 'deviceID', title: 'Device ID', key: 'deviceID' },
  //{ dataIndex: 'createdAt', title: 'Created At', key: 'createdAt' },
  {
    title: 'Things',
    key: 'things',
    render: (_, prf) => (
      <Button type="primary" onClick={goToDetails(prf)} icon="eye">
        Details
      </Button>
    ),
  },
];

type Props = ReturnType<typeof mapStateToProps> &
  ReturnType<typeof mapDispatchToProps>;

const Profile = ({
  profiles,
  onChangeHandler,
  page,
  size,
  loading,
  err,
  itemsPerPage,
  goToDetails,
}: Props) => (
  <div>
    {!!err ? (
      'Whoops'
    ) : (
      <div>
        <Search
          placeholder="input search text"
          onSearch={value => console.log(value)}
          enterButton
        />
        <Table
          onRow={profile => ({
            onClick: () => console.log('im clicked', profile.nym),
          })}
          scroll={{ x: true }}
          dataSource={profiles.map(prf => ({ ...prf, key: prf.nym }))}
          loading={loading}
          columns={columns(goToDetails)}
          onChange={onChangeHandler}
          pagination={{
            current: page,
            total: size,
            pageSize: itemsPerPage,
          }}
        />
      </div>
    )}
  </div>
);
const mapStateToProps = ({ profiles }: IState) => ({
  loading: profiles.profiles.caseOf({ loading: () => true, _: () => false }),
  profiles: profiles.profiles.withDefault(profiles.memo),
  err: profiles.profiles.caseOf({ err: e => e, _: () => undefined }),
  page: profiles.query.page + 1,
  itemsPerPage: profiles.query.itemsPerPage,
  size: profiles.count,
});

const mapDispatchToProps = (dispatch: React.Dispatch<any>) => ({
  goToDetails: ({ nym }: IProfile) => () =>
    dispatch(push(`${appRoutes.Profiles.path}/${nym}`)),
  onChangeHandler: ({ current, pageSize }: PaginationConfig) =>
    dispatch(
      getProfilesAction({
        page: !!current ? current - 1 : undefined,
        itemsPerPage: pageSize,
      })
    ),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
