import {
  Loadable,
  FailableAction,
  PayloadAction,
  payloadActionMaker,
  failableActionMaker,
  implementsInterface,
} from '../utils';
import { LocationChangeAction } from 'connected-react-router';
import { function as FN } from 'fp-ts';
import { isString, isNull } from 'util';

export interface IProfile {
  nym: string;
  firstName: string | null;
  lastName: string | null;
  appCategory: string | null;
  status: string | null;
  scannedCin: string | null;
  cinRecto: string | null;
  cinVerso: string | null;
  userFace: string | null;
  gender: string | null;
  phoneNumber: string | null;
  birthDate: string | null;
  governorate: string | null;
  imei: string | null;
  androidVersion: string | null;
  operatorName: string | null;
  deviceID: string | null;
  createdAt: string | null;
}

export const isProfile = implementsInterface<IProfile>({
  nym: isString,
  androidVersion: FN.or(isString, isNull),
  appCategory: FN.or(isString, isNull),
  birthDate: FN.or(isString, isNull),
  cinRecto: FN.or(isString, isNull),
  cinVerso: FN.or(isString, isNull),
  createdAt: FN.or(isString, isNull),
  deviceID: FN.or(isString, isNull),
  firstName: FN.or(isString, isNull),
  gender: FN.or(isString, isNull),
  governorate: FN.or(isString, isNull),
  imei: FN.or(isString, isNull),
  lastName: FN.or(isString, isNull),
  operatorName: FN.or(isString, isNull),
  phoneNumber: FN.or(isString, isNull),
  scannedCin: FN.or(isString, isNull),
  status: FN.or(isString, isNull),
  userFace: FN.or(isString, isNull),
});
export interface IProfilesState {
  memo: IProfile[];
  profiles: Loadable<IProfile[]>;
  query: { page: number; itemsPerPage: number };
  count: number;
  selectedProfile?: IProfile;
}

// Define profiles actions
export type GetProfiles = PayloadAction<
  'profiles/GET_PROFILES',
  Partial<{ page: number; itemsPerPage: number }>
>;
export const getProfilesAction = payloadActionMaker<GetProfiles>(
  'profiles/GET_PROFILES'
);

export type GetProfilesResult = FailableAction<
  'profiles/GET_PROFILES_RESULT',
  { data: IProfile[]; count: number }
>;
export const [getProfilesResultOk, getProfilesResultErr] = failableActionMaker<
  GetProfilesResult
>('profiles/GET_PROFILES_RESULT');

export type ProfilesAction =
  | GetProfiles
  | GetProfilesResult
  | LocationChangeAction;
