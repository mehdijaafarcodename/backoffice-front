import {
  makeLenses,
  NotRequested,
  makeReducer_,
  Loading,
  Unpack,
} from '../utils';
import {
  IProfilesState,
  ProfilesAction,
  GetProfiles,
  getProfilesResultOk,
  getProfilesResultErr,
  getProfilesAction,
} from './types';
import { effects } from 'redux-saga';
import { IState } from '../store';
import { LocationChangeAction, LOCATION_CHANGE } from 'connected-react-router';
import { matchPath } from 'react-router';
import * as api from './api';
import { absolutePath, appRoutes } from '../home/types';
import { takeLatestAction } from '../utils/saga';

export const profilesInitialState: IProfilesState = {
  memo: [],
  profiles: NotRequested(),
  query: { itemsPerPage: 0, page: 0 },
  count: 0,
  selectedProfile: undefined,
};
const _l = makeLenses(profilesInitialState);

export const profilesReducer = makeReducer_<ProfilesAction, IProfilesState>(
  profilesInitialState,
  {
    'profiles/GET_PROFILES': p => [
      _l.query.modify(q => ({ ...q, ...p })),
      _l.profiles.set(Loading()),
    ],
    'profiles/GET_PROFILES_RESULT': r => [
      _l.profiles.set(r.map(res => res.data)),
      _l.memo.set(r.map(res => res.data).withDefault([])),
      _l.count.set(r.map(res => res.count).withDefault(0)),
    ],
    '@@router/LOCATION_CHANGE': p => s =>
      !matchPath(p.location.pathname, {
        exact: true,
        path: absolutePath(appRoutes)('Profiles'),
      })
        ? { ...profilesInitialState, memo: s.memo }
        : s,
  }
);

function* getProfilesEffect(_: GetProfiles) {
  try {
    const {
      page,
      itemsPerPage,
    }: IProfilesState['query'] = yield effects.select(
      (s: IState) => s.profiles.query
    );
    const data: Unpack<typeof api.getProfiles> = yield effects.call(
      api.getProfiles,
      page,
      itemsPerPage
    );
    yield effects.put(
      getProfilesResultOk({ data: data.profiles, count: data.count })
    );
  } catch (e) {
    yield effects.put(getProfilesResultErr(e));
  }
}

function* changePageEffect({ payload: p }: LocationChangeAction) {
  const match = matchPath(p.location.pathname, {
    path: absolutePath(appRoutes)('Profiles'),
    exact: true,
  });
  if (!!match) {
    yield effects.put(getProfilesAction({}));
  }
}

export function* profilesEffects() {
  yield effects.all([
    takeLatestAction(LOCATION_CHANGE, changePageEffect),
    takeLatestAction('profiles/GET_PROFILES', getProfilesEffect),
  ]);
}
