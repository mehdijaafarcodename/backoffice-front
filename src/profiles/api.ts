import Axios from 'axios';
import { implementsInterface, isArrayOf, extractData } from '../utils';
import { isProfile } from './types';
import { isNumber } from 'util';

export const getProfiles = (page = 0, size = 20) =>
  Axios.get('/backoffice/api/profiles', { params: { page, size } }).then(
    extractData(
      implementsInterface({ count: isNumber, profiles: isArrayOf(isProfile) })
    )
  );
