import * as React from 'react';
import Home from './home/Home';
import { Provider, connect } from 'react-redux';
import store, { history, IState } from './store';
import { ConnectedRouter } from 'connected-react-router';
import SignIn from './authentication/login/SignIn';
import PrivateRoute from './common/PrivateRoute';
import './App.css';
import { Spin } from 'antd';
import { Switch, Route } from 'react-router';

const mapStateToProps = ({ signin: { account } }: IState) => ({ account });

const App = connect(mapStateToProps)(
  ({ account }: ReturnType<typeof mapStateToProps>) =>
    account.caseOf({
      loading: () => <Spin size="large" children={<div />} />,
      _: () => (
        <Switch>
          <Route path="/login" exact={true} component={SignIn} />
          <PrivateRoute path="/" component={Home} />
        </Switch>
      ),
    })
);

export default () => (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App />
    </ConnectedRouter>
  </Provider>
);
