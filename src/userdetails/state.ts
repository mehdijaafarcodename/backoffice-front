import {
  IUserDetailsState,
  UserAction,
  IGetUser,
  getUser,
  IUpdateUser,
  ICreateUser,
  FetchRoles,
  fetchRolesResult,
  fetchRoles,
  getUserOk,
  getUserErr,
  updateUserOk,
  createUserOk,
  createUserErr,
  updateUserErr,
  IUpdateUserResult,
  ICreateUserResult,
} from './types';
import {
  NotRequested,
  Loading,
  Ok,
  makeReducer_,
  makeLenses,
  Unpack,
} from '../utils';
import { IUser, emptyUser, emptyUserDTO } from '../usermanagement/types';
import { all, put, call, select } from 'redux-saga/effects';
import { IState } from '../store';
import {
  LocationChangeAction,
  LOCATION_CHANGE,
  goBack,
} from 'connected-react-router';
import { matchPath } from 'react-router';
import * as api from './api';
import { absolutePath, appRoutes } from '../home/types';
import { takeLatestAction } from '../utils/saga';

export const userDetailsInitialState: IUserDetailsState = {
  user: NotRequested(),
  authorities: NotRequested(),
  updatedUser: emptyUser,
  createdUser: emptyUserDTO,
  createResult: NotRequested(),
  updateResult: NotRequested(),
  formStatus: {},
};

const _l = makeLenses(userDetailsInitialState);

export const userReducer = makeReducer_<UserAction, IUserDetailsState>(
  userDetailsInitialState,
  {
    'userupdate/MODIFY_USER': p => [
      _l.updateResult.set(NotRequested()),
      _l.updatedUser.modify(u => ({ ...u, ...p })),
    ],
    'userupdate/MODIFY_USER_CREATE': p => [
      _l.createResult.set(NotRequested()),
      _l.createdUser.modify(u => ({ ...u, ...p })),
    ],
    'userManagement/FETCH_ROLES': () => s => s,
    'userManagement/FETCH_ROLES_RESULT': _l.authorities.set,
    'userdetails/GET_USER': () => _l.user.set(Loading()),
    'userupdate/UPDATE_USER': () => _l.user.set(Loading()),
    'usercreate/CREATE_USER': () => _l.user.set(Loading()),
    'userdetails/GET_USER_RESULT': p => [
      _l.user.set(p),
      _l.updatedUser.modify(u => p.withDefault(u || emptyUser)),
    ],
    'usercreate/CREATE_USER_RESULT': p => [
      _l.createResult.set(p.map(() => undefined)),
      p.map(() => _l.createdUser.set(emptyUser)).withDefault(s => s),
    ],
    'userupdate/UPDATE_USER_RESULT': p => [
      _l.updateResult.set(p.map(() => undefined)),
      _l.updatedUser.modify(u => p.withDefault(u || emptyUser)),
    ],
    'userdetails/UPDATE_FIELDS': p => _l.formStatus.set(p),
    '@@router/LOCATION_CHANGE': p =>
      ![
        appRoutes['Users management'].routes['Create User'].path,
        appRoutes['Users management'].routes['Edit User'].path,
      ].some(
        path =>
          !!matchPath(p.location.pathname, {
            exact: true,
            path: appRoutes['Users management'].path + path,
          })
      )
        ? [
            _l.formStatus.set({}),
            _l.createdUser.set(emptyUserDTO),
            _l.updatedUser.set(emptyUser),
          ]
        : s => s,
  }
);

function* getRolesEffect(_: FetchRoles) {
  try {
    const data: string[] = yield call(api.getRoles);
    yield put(fetchRolesResult(Ok(data)));
  } catch (e) {
    yield put(fetchRolesResult(e));
  }
}

function* getUserEffect(a: IGetUser) {
  try {
    const existingUser: IUser | undefined = yield select((s: IState) =>
      s.users.users
        .map(ps => ps.find(p => p.login === a.payload))
        .withDefault(undefined)
    );
    if (!!existingUser) {
      yield put(getUserOk(existingUser));
    } else {
      // get profile from the server
      const data: Unpack<typeof api.getUserFromApi> = yield call(
        api.getUserFromApi,
        a.payload
      );
      yield put(getUserOk(data));
    }
  } catch (e) {
    yield put(getUserErr(e));
  }
}

function* updateUserEffect(_: IUpdateUser) {
  try {
    const user: IUser = yield select((s: IState) => s.userDetails.updatedUser);
    const data: Unpack<typeof api.editUser> = yield call(api.editUser, user);
    yield put(updateUserOk(data));
  } catch (e) {
    yield put(updateUserErr(e));
  }
}

function* createUserEffect(_: ICreateUser) {
  try {
    const { id, langKey, ...rest }: IUser = yield select(
      (s: IState) => s.userDetails.createdUser || { id: '', langKey: '' }
    );
    const data: Unpack<typeof api.createUser> = yield call(
      api.createUser,
      rest
    );
    yield put(createUserOk(data));
  } catch (e) {
    console.log(e);
    yield put(createUserErr(e));
  }
}

function* postUpdateOrCreateEffect(a: IUpdateUserResult | ICreateUserResult) {
  // @ts-ignore
  if (a.payload.map(() => true).withDefault(false)) {
    yield put(goBack());
  }
}

function* changePageEffect(a: LocationChangeAction) {
  const newMatch = matchPath<{ login: string }>(a.payload.location.pathname, {
    path: absolutePath(appRoutes)('Users management', 'Create User'),
    exact: true,
  });
  const match = matchPath<{ login: string }>(a.payload.location.pathname, {
    path: absolutePath(appRoutes)('Users management', 'User Details'),
    exact: true,
  });
  const matchEdit = matchPath<{ login: string }>(a.payload.location.pathname, {
    path: absolutePath(appRoutes)('Users management', 'Edit User'),
    exact: true,
  });
  if (!!newMatch) {
    yield put(fetchRoles());
  }
  if (!newMatch && !!match) {
    yield put(getUser(match.params.login));
  }
  if (!!matchEdit) {
    yield put(fetchRoles());
    yield put(getUser(matchEdit.params.login));
  }
}

export function* UserEffects() {
  yield all([
    takeLatestAction(LOCATION_CHANGE, changePageEffect),
    takeLatestAction('userdetails/GET_USER', getUserEffect),
    takeLatestAction('userupdate/UPDATE_USER', updateUserEffect),
    takeLatestAction('usercreate/CREATE_USER', createUserEffect),
    takeLatestAction('userManagement/FETCH_ROLES', getRolesEffect),
    takeLatestAction<IUpdateUserResult, ICreateUserResult>(
      ['userupdate/UPDATE_USER_RESULT', 'usercreate/CREATE_USER_RESULT'],
      postUpdateOrCreateEffect
    ),
  ]);
}
