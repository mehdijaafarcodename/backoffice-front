import * as React from 'react';
import { Form, Input, Button, Checkbox, Modal } from 'antd';
import { IState } from '../store';
import { connect } from 'react-redux';
import { IUserDTO } from '../usermanagement/types';
import { createUser, modifyColumnInCreate, updateFields } from './types';
import { FormComponentProps } from 'antd/lib/form';
import { push } from 'connected-react-router';
import { appRoutes, baseRoutes } from '../home/types';
import { matchPath } from 'react-router';
import HiddenSubmitButton from '../common/HiddenSubmitButton';
import { record as REC } from 'fp-ts';

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = ReturnType<typeof mapDispatchToProps>;
type Props = StateProps & DispatchProps;

const formItemLayout = {
  labelCol: { xs: { span: 24 }, sm: { span: 8 } },
  wrapperCol: { xs: { span: 24 }, sm: { span: 16 } },
};

const UserCreate = Form.create<Props>({
  onValuesChange: (p, _cv, values) => p.onChangeHandler(values),
  onFieldsChange: (p, _f, fields) => p.updateFields(fields),
  mapPropsToFields: ({ user, fields }) =>
    REC.mapWithKey(
      (user as unknown) as { [_: string]: string | null | boolean | string[] },
      // @ts-ignore
      (k, value) => Form.createFormField({ ...fields[k], value })
    ),
})(
  ({
    authorities,
    handleSubmit,
    form: { getFieldDecorator },
    goBack,
    visible,
  }: Props & FormComponentProps) => (
    <Modal
      visible={visible}
      title="User Create"
      onCancel={goBack}
      footer={[
        <Button type="primary" icon="rollback" ghost onClick={goBack}>
          Back
        </Button>,
        <Button type="primary" icon="save" onClick={handleSubmit}>
          Save
        </Button>,
      ]}
    >
      <Form onSubmit={handleSubmit}>
        <Form.Item {...formItemLayout} label="Login">
          {getFieldDecorator('login', {
            rules: [{ required: true, message: 'Please input your login!' }],
          })(<Input placeholder="Login" />)}
        </Form.Item>
        <Form.Item {...formItemLayout} label="FirstName">
          {getFieldDecorator('firstName', {})(
            <Input placeholder="FirstName" />
          )}
        </Form.Item>
        <Form.Item {...formItemLayout} label="LastName">
          {getFieldDecorator('lastName', {})(<Input placeholder="LastName" />)}
        </Form.Item>
        <Form.Item {...formItemLayout} label="Email">
          {getFieldDecorator('email', {
            rules: [
              {
                type: 'email',
                message: 'The input is not valid E-mail!',
              },
              {
                required: true,
                message: 'Please input your E-mail!',
              },
            ],
          })(<Input placeholder="Email" />)}
        </Form.Item>
        <Form.Item {...formItemLayout} label="Authorities">
          {getFieldDecorator('authorities')(
            <Checkbox.Group options={authorities.map(t => t).withDefault([])} />
          )}
        </Form.Item>
        <HiddenSubmitButton />
      </Form>
    </Modal>
  )
);

const mapStateToProps = ({
  userDetails: { authorities, createdUser, formStatus },
  router: { location },
}: IState) => ({
  visible: !!matchPath(
    location.pathname,
    baseRoutes['Users management'].path +
      baseRoutes['Users management'].routes['Create User'].path
  ),
  authorities,
  user: createdUser,
  fields: formStatus,
});

const mapDispatchToProps = (dispatch: React.Dispatch<any>) => ({
  goBack: () => dispatch(push(appRoutes['Users management'].path)),
  onChangeHandler: (user: IUserDTO) => dispatch(modifyColumnInCreate(user)),
  handleSubmit: (e: React.FormEvent) => {
    e.preventDefault();
    dispatch(createUser());
  },
  updateFields: (o: object) => dispatch(updateFields(o)),
});

export default connect<StateProps, DispatchProps, {}, IState>(
  mapStateToProps,
  mapDispatchToProps
)(UserCreate);
