import * as React from 'react';
import { IState } from '../store';
import { connect } from 'react-redux';
import { Row, Col, Button, Tag, Modal } from 'antd';
import { push } from 'connected-react-router';
import { baseRoutes } from '../home/types';
import { matchPath } from 'react-router';

type Props = ReturnType<typeof mapStateToProps> & typeof mapDispatchToProps;

const UserDetails = ({ visible, userDetails, goBack }: Props) => (
  <Modal
    onCancel={goBack}
    title={`User details: ${userDetails.map(u => u.login).withDefault('')}`}
    visible={visible}
    footer={[
      <Button type="primary" ghost icon="rollback" onClick={goBack}>
        Back
      </Button>,
    ]}
  >
    <h2>Details User</h2>
    <Row>
      <Col span={8}>
        <h4>ID :</h4>
        <p>{userDetails.map(u => u.id).withDefault(0)}</p>
      </Col>
      <Col span={8}>
        <h4>Login :</h4>
        <p>{userDetails.map(u => u.login).withDefault('')}</p>
      </Col>
    </Row>
    <Row>
      <Col span={8}>
        <h4>FirstName :</h4>
        <p>{userDetails.map(u => u.firstName).withDefault('')}</p>
      </Col>
      <Col span={8}>
        <h4>LastName :</h4>
        <p>{userDetails.map(u => u.lastName).withDefault('')}</p>
      </Col>
    </Row>
    <Row>
      <Col span={8}>
        <h4>Email :</h4>
        <p>{userDetails.map(u => u.email).withDefault('')}</p>
      </Col>
      <Col span={8}>
        <h4>Status :</h4>
        {userDetails.map(u => u.activated).withDefault(false) ? (
          <Tag color="#87d068">Activated</Tag>
        ) : (
          <Tag color="#FF0000">Deactivated</Tag>
        )}
      </Col>
    </Row>
    <Row>
      <Col span={8}>
        <h4>Language key</h4>
        {userDetails.map(u => u.langKey).withDefault('') === null ? (
          ''
        ) : (
          <Tag color="red">
            {userDetails.map(u => u.langKey).withDefault('')}
          </Tag>
        )}
      </Col>
      <Col span={8}>
        <h4>Authorities</h4>
        {userDetails
          .map(u =>
            (u.authorities || []).map(ts => (
              <Tag key={ts} color="blue">
                {ts}
              </Tag>
            ))
          )
          .withDefault([])}
      </Col>
    </Row>
  </Modal>
);

const mapStateToProps = ({
  userDetails,
  router: {
    location: { pathname },
  },
}: IState) => ({
  userDetails: userDetails.user,
  visible: !!matchPath(
    pathname,
    baseRoutes['Users management'].path +
      baseRoutes['Users management'].routes['User Details'].path
  ),
});
const mapDispatchToProps = {
  goBack: () => push(baseRoutes['Users management'].path),
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserDetails);
