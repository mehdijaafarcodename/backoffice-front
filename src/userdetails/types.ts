import {
  Loadable,
  Result,
  failableActionMaker,
  bothWithMeta,
  PayloadAction,
  payloadActionMaker,
} from '../utils';
import { IUser, IUserDTO } from '../usermanagement/types';
import { LocationChangeAction } from 'connected-react-router';

export interface IUserDetailsState {
  user: Loadable<IUser>;
  authorities: Loadable<string[]>;
  updatedUser: IUser;
  createdUser: IUserDTO;
  createResult: Loadable<undefined>;
  updateResult: Loadable<undefined>;
  formStatus: object;
}

export interface IGetUser {
  type: 'userdetails/GET_USER';
  payload: string;
}

export const getUser = (login: string): IGetUser => ({
  type: 'userdetails/GET_USER',
  payload: login,
});

export interface IGetUserResult {
  type: 'userdetails/GET_USER_RESULT';
  payload: Result<IUser>;
}
export const [getUserOk, getUserErr] = failableActionMaker<IGetUserResult>(
  'userdetails/GET_USER_RESULT'
);

//-------------------get authorities

export interface FetchRoles {
  type: 'userManagement/FETCH_ROLES';
}
export const fetchRoles = (): FetchRoles => ({
  type: 'userManagement/FETCH_ROLES',
});

export interface FetchRolesResult {
  type: 'userManagement/FETCH_ROLES_RESULT';
  payload: Result<string[]>;
}

export const fetchRolesResult = (v: Result<string[]>): FetchRolesResult => ({
  type: 'userManagement/FETCH_ROLES_RESULT',
  payload: v,
});
//-------------------create

export interface ICreateUser {
  type: 'usercreate/CREATE_USER';
}

export const createUser = (): ICreateUser => ({
  type: 'usercreate/CREATE_USER',
});

export interface ICreateUserResult {
  type: 'usercreate/CREATE_USER_RESULT';
  payload: Result<IUserDTO>;
}
export const [createUserOk, createUserErr] = bothWithMeta(
  { successMessage: 'User created', errorMessage: 'Error creating user' },
  failableActionMaker<ICreateUserResult>('usercreate/CREATE_USER_RESULT')
);
//-----------------------------

export interface IModifyUser {
  type: 'userupdate/MODIFY_USER';
  payload: IUser;
}

export const modifyUser = (user: IUser): IModifyUser => ({
  type: 'userupdate/MODIFY_USER',
  payload: user,
});

//-------------------------

export interface IModifyColumnInCreate {
  type: 'userupdate/MODIFY_USER_CREATE';
  payload: IUserDTO;
}

export const modifyColumnInCreate = (
  user: IUserDTO
): IModifyColumnInCreate => ({
  type: 'userupdate/MODIFY_USER_CREATE',
  payload: user,
});
//-------------------update

export interface IUpdateUser {
  type: 'userupdate/UPDATE_USER';
}

export const updateUser = (): IUpdateUser => ({
  type: 'userupdate/UPDATE_USER',
});

export interface IUpdateUserResult {
  type: 'userupdate/UPDATE_USER_RESULT';
  payload: Result<IUser>;
}

export const [updateUserOk, updateUserErr] = bothWithMeta(
  { successMessage: 'User updated', errorMessage: 'Error updating user' },
  failableActionMaker<IUpdateUserResult>('userupdate/UPDATE_USER_RESULT')
);

export type UpdateFields = PayloadAction<'userdetails/UPDATE_FIELDS', object>;
export const updateFields = payloadActionMaker<UpdateFields>(
  'userdetails/UPDATE_FIELDS'
);

export type UserAction =
  | FetchRoles
  | FetchRolesResult
  | IGetUser
  | IGetUserResult
  | ICreateUser
  | ICreateUserResult
  | IUpdateUser
  | IUpdateUserResult
  | IModifyUser
  | IModifyColumnInCreate
  | UpdateFields
  | LocationChangeAction;
