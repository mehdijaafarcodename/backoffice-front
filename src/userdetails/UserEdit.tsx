import * as React from 'react';
import { Form, Input, Button, Checkbox, Modal } from 'antd';
import { IState } from '../store';
import { connect } from 'react-redux';
import { FormComponentProps } from 'antd/lib/form';
import { modifyUser, updateUser } from './types';
import { record as REC } from 'fp-ts';
import { emptyUser } from '../usermanagement/types';
import { function as FN } from 'fp-ts';
import { push } from 'connected-react-router';
import { baseRoutes } from '../home/types';
import { matchPath } from 'react-router';
import HiddenSubmitButton from '../common/HiddenSubmitButton';

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;
type Props = StateProps & DispatchProps;

const formItemLayout = {
  labelCol: { xs: { span: 24 }, sm: { span: 8 } },
  wrapperCol: { xs: { span: 24 }, sm: { span: 16 } },
};

const UserEdit = Form.create<Props>({
  onValuesChange: ({ modifyUser }, newValues) => modifyUser(newValues),
  mapPropsToFields: props =>
    REC.map(props.userDetails.withDefault(emptyUser), v =>
      Form.createFormField({ value: v })
    ),
})(
  ({
    goBack,
    userDetails,
    authorities,
    updateUser,
    form: { getFieldDecorator },
    visible,
  }: Props & FormComponentProps) => (
    <Modal
      title={`Edit User: ${userDetails.map(u => u.login).withDefault('')}`}
      onCancel={goBack}
      footer={[
        <Button type="primary" ghost icon="rollback" onClick={goBack}>
          Back
        </Button>,
        <Button
          loading={userDetails.caseOf({
            loading: () => true,
            _: () => false,
          })}
          icon="save"
          type="primary"
          onClick={updateUser}
        >
          Save
        </Button>,
      ]}
      visible={visible}
    >
      <Form onSubmit={updateUser}>
        <Form.Item {...formItemLayout} label="Login">
          {getFieldDecorator('login', {
            rules: [
              {
                required: true,
                message: 'Please input your login!',
              },
            ],
          })(<Input />)}
        </Form.Item>
        <Form.Item {...formItemLayout} label="FirstName">
          {getFieldDecorator('firstName')(<Input />)}
        </Form.Item>
        <Form.Item {...formItemLayout} label="LastName">
          {getFieldDecorator('lastName')(<Input />)}
        </Form.Item>
        <Form.Item {...formItemLayout} label="Email">
          {getFieldDecorator('email', {
            rules: [
              {
                type: 'email',
                message: 'The input is not valid E-mail!',
              },
              {
                required: true,
                message: 'Please input your E-mail!',
              },
            ],
          })(<Input />)}
        </Form.Item>
        <Form.Item {...formItemLayout} label="Status">
          {getFieldDecorator('activated')(
            <Checkbox
              value={userDetails.map(u => u.activated).withDefault(false)}
            >
              Activated
            </Checkbox>
          )}
        </Form.Item>
        <Form.Item {...formItemLayout} label="Language key">
          {getFieldDecorator('langKey')(<Input />)}
        </Form.Item>
        <Form.Item {...formItemLayout} label="Authorities">
          {getFieldDecorator('authorities')(
            <Checkbox.Group
              options={authorities.map(t => t).withDefault([])}
              value={userDetails.map(u => u.authorities).withDefault([])}
            />
          )}
        </Form.Item>
        <HiddenSubmitButton />
      </Form>
    </Modal>
  )
);

const mapStateToProps = ({ userDetails }: IState) => ({
  visible: !!matchPath(
    location.pathname,
    baseRoutes['Users management'].path +
      baseRoutes['Users management'].routes['Edit User'].path
  ),
  userDetails: userDetails.user,
  authorities: userDetails.authorities,
});

const mapDispatchToProps = {
  modifyUser,
  goBack: () => push(baseRoutes['Users management'].path),
  updateUser: FN.pipe(
    (e: React.FormEvent) => e.preventDefault(),
    updateUser
  ),
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserEdit);
