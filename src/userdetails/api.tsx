import axios from 'axios';
import { IUser, IUserDTO, isUser } from '../usermanagement/types';
import { isArrayOf, extractData } from '../utils';
import { isString } from 'util';

export const getRoles = () =>
  axios
    .get(`/uaa/api/users/authorities`)
    .then(extractData(isArrayOf(isString)));

export const getUserFromApi = (login: string) =>
  axios.get(`/uaa/api/users/${login}`).then(extractData(isUser));

export const editUser = (user: IUser) =>
  axios.put('/uaa/api/users/', user).then(extractData(isUser));

export const createUser = (user: IUserDTO) =>
  axios.post('/uaa/api/users/', user).then(extractData(isUser));

export const deleteUser = (login: string) =>
  axios.delete(`/uaa/api/users/${login}`);
