import * as React from 'react';
import { Switch, Link, Route } from 'react-router-dom';
import { Layout, Menu, Icon, Button } from 'antd';
import '../App.css';
import styled from 'styled-components/macro';
import PrivateRoute from '../common/PrivateRoute';
import { connect } from 'react-redux';
import { logout, ROLE_ADMIN } from '../authentication/login/types';
import { IState } from '../store';
import { record as REC } from 'fp-ts';
import { flattenedRoutes, appRoutes } from './types';
import { splitPath } from '../utils';

const StyledHeader = styled(Layout.Header)`
  background: #fff;
  padding: 0 16px;
  display: flex;
  flex-director: row;
  justify-content: space-between;
  align-items: center;
  box-shadow: 0 2px 8px #f0f1f2;
  z-index: 1;
`;

const Title = styled.h2`
  margin: 0;
`;

const StyledContent = styled(Layout.Content)`
  overflow: auto;
  background: #fff;
`;

const ContentChild = styled.div`
  padding: 16px;
  background: #fff;
  min-height: 360;
`;

const StyledFooter = styled(Layout.Footer)`
  text-align: right;
`;

const Logo = styled.div`
  height: 64px;
`;

type Props = typeof mapDispatchToProps & ReturnType<typeof mapStateToProps>;

const App = ({ roles, location, logout }: Props) => (
  <Layout>
    <Layout.Sider collapsible breakpoint="lg">
      <Logo />
      <Menu
        theme="dark"
        mode="inline"
        defaultSelectedKeys={splitPath(location.pathname)}
      >
        {REC.toArray(appRoutes)
          .filter(([_, { requiredRole }]) => roles.includes(requiredRole))
          .map(([n, { path, icon }]) => (
            <Menu.Item key={path}>
              <Link to={path}>
                {!!icon && <Icon type={icon} />}
                <span>{n}</span>
              </Link>
            </Menu.Item>
          ))}
      </Menu>
    </Layout.Sider>
    <Layout>
      <StyledHeader>
        <Title>
          Backoffice -{' '}
          <Switch>
            {REC.toArray(flattenedRoutes).map(([n, p]) => (
              <Route key={p.path} path={p.path} exact render={() => n} />
            ))}
          </Switch>
        </Title>
        <Button icon="logout" onClick={logout}>
          Logout
        </Button>
      </StyledHeader>
      <StyledContent>
        <ContentChild>
          <Switch>
            {REC.toArray(appRoutes).map(([_, p]) => (
              <PrivateRoute key={p.path} {...p} />
            ))}
            {REC.toArray(appRoutes.Profiles.routes).map(([_, p]) => (
              <PrivateRoute key={appRoutes.Profiles.path + p.path} {...p} />
            ))}
            <PrivateRoute>
              <p>
                {roles.includes(ROLE_ADMIN)
                  ? 'Some welcome message'
                  : 'You are not authorized to use this service.'}
              </p>
            </PrivateRoute>
          </Switch>
        </ContentChild>
      </StyledContent>
      <StyledFooter>TPay ©2018 Created by Digitus</StyledFooter>
    </Layout>
  </Layout>
);

const mapStateToProps = ({
  signin: { account },
  router: { location },
}: IState) => ({
  location,
  roles: account.map(a => a.authorities).withDefault([]),
});
const mapDispatchToProps = { logout: logout };
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
