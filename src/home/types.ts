import Profile from '../profiles/Profile';
import UserManagement from '../usermanagement/UserManagement';
import UserCreate from '../userdetails/UserCreate';
import UserDetails from '../userdetails/UserDetails';
import UserEdit from '../userdetails/UserEdit';
import ProfileDetails from '../profiledetails/ProfileDetails';
import { RouteComponentProps } from 'react-router';
import { record as REC } from 'fp-ts';
import { ROLE_ADMIN } from '../authentication/login/types';

export interface IPathDefinition {
  path: string;
  icon?: string;
  requiredRole?: string;
  exact?: boolean;
  component:
    | React.ComponentType<RouteComponentProps<any>>
    | React.ComponentType<any>;
}

export interface IRoute {
  [name: string]: IPathDefinition & { routes?: IRoute };
}

export const appRoutes = {
  Profiles: {
    icon: 'user',
    path: '/profile',
    component: Profile,
    requiredRole: ROLE_ADMIN,
    exact: true,
    routes: {
      'Profile Details': {
        path: '/:nym',
        component: ProfileDetails,
        requiredRole: ROLE_ADMIN,
      },
    },
  },
  'Users management': {
    icon: 'team',
    path: '/users-management',
    component: UserManagement,
    requiredRole: ROLE_ADMIN,
    routes: {
      'Create User': {
        path: '/new',
        component: UserCreate,
        requiredRole: ROLE_ADMIN,
      },
      'User Details': {
        path: '/details/:login',
        component: UserDetails,
        requiredRole: ROLE_ADMIN,
      },
      'Edit User': {
        path: '/edit/:login',
        component: UserEdit,
        requiredRole: ROLE_ADMIN,
      },
    },
  },
};

export const baseRoutes = {
  Profiles: {
    icon: 'user',
    path: '/profile',
    requiredRole: ROLE_ADMIN,
    exact: true,
    routes: {
      'Profile Details': { path: '/:nym', requiredRole: ROLE_ADMIN },
    },
  },
  'Users management': {
    icon: 'team',
    path: '/users-management',
    requiredRole: ROLE_ADMIN,
    routes: {
      'Create User': { path: '/new', requiredRole: ROLE_ADMIN },
      'User Details': { path: '/details/:login', requiredRole: ROLE_ADMIN },
      'Edit User': { path: '/edit/:login', requiredRole: ROLE_ADMIN },
    },
  },
};

const flatten = (r: IRoute): { [_: string]: IPathDefinition } =>
  REC.reduceWithKey(r, {} as IRoute, (k, p, c) => ({
    ...p,
    [k]: c,
    ...REC.map(flatten(c.routes || {}), v => ({ ...v, path: c.path + v.path })),
  }));

export const flattenedRoutes = flatten(appRoutes);

interface absolutePathOverload {
  <R extends IRoute>(rs: R): <
    K1 extends keyof R,
    K2 extends keyof R[K1]['routes']
  >(
    ...ks: [K1, K2?]
  ) => string;
}
export const absolutePath: absolutePathOverload = (rs: IRoute) => (
  ...ks: any[]
) =>
  ks.reduce(
    (p, c) =>
      !!p[c]
        ? ({ ...p[c].routes, absolute: p.absolute + p[c].path } as IRoute & {
            absolute: string;
          })
        : p,
    { ...rs, absolute: '' } as IRoute & { absolute: string }
  ).absolute;
